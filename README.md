# AWS Secret Rotator

*This is a simple repo to demonstrate how to write your own lambda to rotate
your secret using Python and serverless*

AWS has a unified interface to manage your applications secrets: fittingly it's
called Secrets Manager. The basic idea behind it is that it's a simple key-value
pair storage system. You give it the name of the secret you want, and it gives
you the secret that corresponds to it.

One nice thing with Secrets manager, is that it has built-in support to rotate
your secrets. To rotate your secret you need to write a lambda that updates it.
AWS provides a handful of lambdas to rotate RDS passwords by default, but what
if you need a custom one? Well we can write a simple one using serverless.

## Secret Updating
Each time AWS calls your lambda, it will do so with the following parameters:

```
{
    "Step": "request.type",
    "SecretId": "string",
    "ClientRequestID": "string",
}
```

Before we can start updating secrets, we need to see the process that AWS uses.
There are four steps that AWS will call your lambda with, each step has a
specific purpose.

- createSecret
- setSecret
- testSecret
- finishSecret

### createSecret
This step is the first step that AWS will call your lambda with. The purpose of
this step is to stage your secret. At the end of this step, the `AWSPENDING`
secret should be set.

### setSecret
Now that we have a new secret staged, we need to set it in at the
application-level. For example, if we were updating a database password, at this
step, we would log into the RDBMS and set the password with the value in
`AWSPENDING`.

### testSecret
The purpose of this step is to validate that the soon-to-be secret will
work. Here we pretend like we're the application and use the secret the same
way.

### finishSecret
Finally, after staging the secret, setting the secret at the application level,
and testing it, we are ready to promote it to `AWSCURRENT`. After this, our
secret will be good to go.

### The Simple Lambda
The lambda itself is simple. The main function reacts to the different steps
that it can be called with and acts accordingly.

An important part in the serverless config is the `AWS::Lambda::Permission`
resource. This important part lets secretsmanager call the lambda to rotate the
password. More info on the permissions required can be found
[here](https://docs.aws.amazon.com/secretsmanager/latest/userguide/rotating-secrets-required-permissions.html)

## Rotating
Let's create our secret and see how the rotation works.

Let's begin by creating out secret, we can do that with the CLI

```
aws secretsmanager create-secret --name SimpleSecret \
    --description "Sinple secret" \
    --secret-string file://name.json
```

Now that we have everything in place, it's time to rotate the secret and set up
a schedule for it. Assuming we have a secret created and the serverless lambda
deployed we can rotate the secret with the AWS CLI and set up automatic rotation
every day

```
aws secretsmanager rotate-secret --secret-id SimpleSecret \
    --rotation-lambda-arn $(aws lambda list-functions | jq -r '.Functions[] | select(.FunctionName == "simple-lambda").FunctionArn')
    --rotation-rules AutomaticallyAfterDays=1
```

And to verify that it did work, we can retrieve the updated value

```
aws secretsmanager get-secret-value --secret-id SimpleSecret | jq -r '.SecretString'
```

In my case, my output was `{"name": "Isaac Thompson"}`
