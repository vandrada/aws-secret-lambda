import json
import logging

import boto3

from faker import Faker

SECRETSMANAGER = boto3.client("secretsmanager")


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def create_secret(arn, token):
    """
    Handles the createSecret step: Creates a new name, staged in AWSPENDING
    """
    fake = Faker()
    pending_secret = fake.name()
    secret_dict = {"name": pending_secret}
    SECRETSMANAGER.put_secret_value(
        SecretId=arn,
        ClientRequestToken=token,
        SecretString=json.dumps(secret_dict),
        VersionStages=["AWSPENDING"],
    )

    logger.info(
        f"createSecret: Successfully put secret for ARN {arn} and version {token}."
    )


def set_secret(arn, token):
    """
    Handles the setSecret step: Sets the secret staged in AWSPENDING and created
    in the createSecret step.

    This step, at least from the docs, is meant to actually set the secret in
    the external system, e.g if we're updating a DB password, this would log
    in to the db and change the password to the AWSPENDING one set above.

    All of that is a long-winded way of saying that there is nothing to do here,
    since the secret exists only in secretsmanager
    """
    pass


def test_secret(arn, token):
    """
    Handles the testSecret step: Tests the AWSPENDING secret like an application
    would.
    """
    pass


def finish_secret(arn, token):
    """
    Handles the finishSecret step: finalizes the AWSPENDING secret by promoting
    it to AWSCURRENT
    """
    metadata = SECRETSMANAGER.describe_secret(SecretId=arn)
    current_version = None
    for version in metadata["VersionIdsToStages"]:
        if "AWSCURRENT" in metadata["VersionIdsToStages"][version]:
            if version == token:
                # The correct version is already marked as current, return
                logger.info(
                    f"finishSecret: Version{version} already marked as AWSCURRENT for {arn}"
                )
                return
            current_version = version
            break

    # Finalize by staging the secret version current
    SECRETSMANAGER.update_secret_version_stage(
        SecretId=arn,
        VersionStage="AWSCURRENT",
        MoveToVersionId=token,
        RemoveFromVersionId=current_version,
    )

    logger.info(f"secret promoted to CURRENT for {arn}")

    # delete the PENDING version
    for version in metadata["VersionIdsToStages"]:
        if "AWSPENDING" in metadata["VersionIdsToStages"][version]:
            pending_version = version
            break
    if pending_version:
        SECRETSMANAGER.update_secret_version_stage(
            SecretId=arn, VersionStage="AWSPENDING", RemoveFromVersionId=pending_version
        )


def handler(event, context):
    arn = event["SecretId"]
    token = event["ClientRequestToken"]
    step = event["Step"]

    if step == "createSecret":
        create_secret(arn, token)
    elif step == "setSecret":
        set_secret(arn, token)
    elif step == "testSecret":
        test_secret(arn, token)
    elif step == "finishSecret":
        finish_secret(arn, token)
    else:
        msg = f"Invalid step parameter {step}"
        logger.error(msg)
        raise ValueError(msg)
